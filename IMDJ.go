package casino

import (
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/bet365/jingo"
	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
)

type imdjResponse struct {
	MemberCode   string `json:"memberCode"`
	CurrencyCode string `json:"CurrencyCode"`
	IPAddress    string `json:"IPAddress"`
	StatusCode   int    `json:"statusCode"`
	StatusDesc   string `json:"statusDesc"`
}

const IMDJ string = "IMDJ"

func imdjPack(key string) string {

	has := md5.Sum([]byte(key))
	key = fmt.Sprintf("%x", has)

	lens := len(key) / 2
	md5raw := ""
	for i := 0; i < lens; i++ {
		hexByte, _ := hex.DecodeString(substring(key, i*2, (i*2)+2))
		md5raw = md5raw + string(hexByte)
	}

	//3DES的密钥需要24位
	keyByte := make([]byte, 24) //设置加密数组
	keyB := []byte(md5raw)

	copy(keyByte[0:], keyB[0:16])
	copy(keyByte[16:], keyB[0:8])

	nt := time.Now().Unix()
	preDayTime := nt - 12*3600
	nano := time.Now().UnixNano()
	nanoStr := strconv.FormatInt(nano, 10)
	timeStr := time.Unix(preDayTime, 0).Format(dateF)

	timeStamp := timeStr + "." + substring(nanoStr, len(nanoStr)-3, len(nanoStr))
	aesByte, _ := tripleDesEncrypt([]byte(timeStamp), keyByte)

	return base64.StdEncoding.EncodeToString(aesByte)
}

// 登录回调
func ImdjCallback(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	state := 0
	msg := "token null"
	username := ""
	ip := ""

	token, ok := param["token"].(string)

	if ok {
		msg = "token error"
		tokenArr := strings.Split(token, "-")
		if len(tokenArr) == 2 {
			username = tokenArr[1]
		}

		arr := strings.Split(tokenArr[0], "_")
		if len(arr) == 2 {
			ip = arr[1]
		}
	}

	if username != "" {

		msg = "Success"
		state = 100

	}

	res := imdjResponse{
		MemberCode:   username,
		CurrencyCode: param["currency"].(string),
		IPAddress:    ip,
		StatusCode:   state,
		StatusDesc:   msg,
	}

	enc := jingo.NewStructEncoder(imdjResponse{})

	buf := jingo.NewBufferFromPool()

	enc.Marshal(&res, buf)

	b := buf.String()
	buf.ReturnToPool()

	l := log_t{
		Requesturl:  "",
		Requestbody: token,
		Statuscode:  state,
		Name:        IMDJ,
		Level:       "info",
		Body:        b,
		Err:         "",
	}

	err := zlog.Post(tag, l)
	if err != nil {
		fmt.Printf("Push IMDJ log is error: %s \n", err.Error())
	}

	return Success, b
}

// 注册
func imdjReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	timeStamp := imdjPack(param["key"].(string))

	token := fmt.Sprintf("IMDJ_%s-%s", param["ip"].(string), param["username"].(string))

	args := map[string]string{
		"timeStamp": timeStamp,
		"token":     token,
	}

	jsonStr, _ := jettison.Marshal(args)

	u, _ := url.Parse(param["api"].(string))

	requestURI := fmt.Sprintf("%s/api/login", param["api"].(string))

	headers := map[string]string{
		"Content-Type": "application/json",
		"Accept":       "application/json",
		"Host":         u.Host,
		"Connection":   "Keep-Alive",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, jsonStr, IMDJ, requestURI, headers)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	if v.GetFloat64("StatusCode") == 0 {
		return Success, "success"
	}

	return Failure, string(v.GetStringBytes("StatusDesc"))
}

func imdjLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	str := imdjPack(param["key"].(string))

	token := fmt.Sprintf("IMDJ_%s-%s", param["ip"].(string), param["username"].(string))

	args := map[string]string{
		"timeStamp": str,
		"token":     token,
	}

	jsonStr, _ := jettison.Marshal(args)

	u, _ := url.Parse(param["api"].(string))

	requestURI := fmt.Sprintf("%s/api/login", param["api"].(string))

	headers := map[string]string{
		"Content-Type": "application/json",
		"Accept":       "application/json",
		"Host":         u.Host,
		"Connection":   "Keep-Alive",
	}
	statusCode, body, err := httpPostWithPushLog(zlog, jsonStr, IMDJ, requestURI, headers)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	if v.GetFloat64("StatusCode") != 0 {
		return Failure, string(v.GetStringBytes("StatusDesc"))
	}

	lang := map[string]string{
		"1": "ZH",
	}

	urlSuffix := "mobile.aspx"

	if param["deviceType"].(string) == "1" {

		urlSuffix = "esport.aspx"
	}

	loginUrl := fmt.Sprintf("%s/%s?timestamp=%s&token=%s&LanguageCode=%s", param["login_url"].(string), urlSuffix, str, token, lang[param["lang"].(string)])

	return Success, loginUrl

}

func imdjBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	str := imdjPack(param["key"].(string))

	args := map[string]string{
		"timeStamp":  str,
		"memberCode": param["username"].(string),
		// "currencyCode": param["currency"].(string),
	}

	jsonStr, _ := jettison.Marshal(args)

	u, _ := url.Parse(param["api"].(string))

	requestURI := fmt.Sprintf("%s/api/getsinglememberbalance", param["api"].(string))

	headers := map[string]string{
		"Content-Type": "application/json",
		"Accept":       "application/json",
		"Host":         u.Host,
		"Connection":   "Keep-Alive",
	}
	statusCode, body, err := httpPostWithPushLog(zlog, jsonStr, IMDJ, requestURI, headers)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	if v.GetFloat64("StatusCode") == 0 {
		return Success, GetBalanceFromFloat(v.GetFloat64("balanceCredit"))
	}

	return Failure, string(v.GetStringBytes("StatusDesc"))
}

func imdjTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	str := imdjPack(param["key"].(string))

	actionTypeId := "deposit"

	if param["type"].(string) == "out" {
		actionTypeId = "withdrawal"
	}

	token := fmt.Sprintf("IMDJ_%s-%s", param["ip"].(string), param["username"].(string))

	args := map[string]string{
		"timeStamp":    str,
		"memberCode":   param["username"].(string),
		"currencyCode": param["currency"].(string),
		"amount":       param["amount"].(string),
		"token":        token,
		"transferID":   param["id"].(string),
	}

	jsonStr, _ := jettison.Marshal(args)

	u, _ := url.Parse(param["api"].(string))

	requestURI := fmt.Sprintf("%s/api/%s", param["api"].(string), actionTypeId)

	headers := map[string]string{
		"Content-Type": "application/json",
		"Accept":       "application/json",
		"Host":         u.Host,
		"Connection":   "Keep-Alive",
	}
	statusCode, body, err := httpPostWithPushLog(zlog, jsonStr, IMDJ, requestURI, headers)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	if v.GetFloat64("StatusCode") == 0 {
		return Success, param["id"].(string)
	}

	return Failure, string(v.GetStringBytes("StatusDesc"))
}

//转账确认
func imdjConfirm(zlog *fluent.Fluent, param map[string]interface{}) bool {

	return false
}
