package casino

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
)

const (
	SGCP = "SGCP"
)

func sgcpPack(key string, args map[string]string) string {

	param := url.Values{}
	keys := []string{"agentID", "root", "username"}
	strMake := []string{}

	for k, v := range args {
		param.Set(k, v)
	}

	for _, v := range keys {
		strMake = append(strMake, v+"="+args[v])
	}

	strMake = append(strMake[0:], key)
	md5Str := strings.Join(strMake, "&")

	param.Set("hash", getMD5Hash(md5Str))

	return param.Encode()
}

func sgcpReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {
	var p fastjson.Parser

	args := map[string]string{
		"agentID":  param["agent_id"].(string),
		"root":     param["merchant"].(string),
		"username": param["username"].(string),
	}

	paramStr := sgcpPack(param["agent_key"].(string), args)
	requestURI := fmt.Sprintf("%s/api/login?%s", param["api"].(string), paramStr)

	header := map[string]string{
		"Content-Type": "application/json",
	}

	jsonData := map[string]string{
		"defaultBgColor": "white",
		"defaultColor":   "blue",
		"tesing":         "0",
	}

	if param["tester"].(string) == "0" {
		jsonData["tesing"] = "1"
	}
	jsonStr, _ := jettison.Marshal(jsonData)

	statusCode, body, err := httpPostWithPushLog(zlog, jsonStr, SGCP, requestURI, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	if !v.GetBool("success") {
		return Failure, string(v.GetStringBytes("message"))
	}

	vv := v.Get("result")

	return Success, string(vv.GetStringBytes("session"))
}

func sgcpLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	res, sess := sgcpReg(zlog, param)
	if res == Failure {
		return Failure, sess
	}

	//桌面版
	if param["deviceType"].(string) == "1" {
		return Success, fmt.Sprintf("%s/member/index?_OLID_=%s", param["login_url"].(string), sess)
	}

	return Success, fmt.Sprintf("%s/mobile/member/index?_OLID_=%s", param["login_url"].(string), sess)
}

func sgcpBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"agentID":  param["agent_id"].(string),
		"root":     param["merchant"].(string),
		"username": param["username"].(string),
	}

	paramStr := sgcpPack(param["agent_key"].(string), args)
	requestURI := fmt.Sprintf("%s/api/account?%s", param["api"].(string), paramStr)

	u, _ := url.Parse(param["api"].(string))
	header := map[string]string{
		"Host":         u.Host,
		"Content-Type": "application/json",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, nil, SGCP, requestURI, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	if !v.GetBool("success") {
		return Failure, string(v.GetStringBytes("message"))
	}

	vv := v.Get("result")

	return Success, GetBalanceFromFloat(vv.GetFloat64("balance"))
}

func sgcpTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	amount, _ := strconv.ParseFloat(param["amount"].(string), 64)
	if param["type"].(string) == "out" {
		amount = amount * -1.00
	}

	var p fastjson.Parser

	args := map[string]string{
		"agentID":  param["agent_id"].(string),
		"root":     param["merchant"].(string),
		"username": param["username"].(string),
	}

	jsonData := map[string]string{
		"amount": fmt.Sprintf("%.2f", amount),
		"unique": param["id"].(string),
	}

	jsonStr, _ := jettison.Marshal(jsonData)

	paramStr := sgcpPack(param["agent_key"].(string), args)
	requestURI := fmt.Sprintf("%s/api/transaction?%s", param["api"].(string), paramStr)

	header := map[string]string{
		"Content-Type": "application/json",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, jsonStr, SGCP, requestURI, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	if !v.GetBool("success") {
		return Failure, string(v.GetStringBytes("message"))
	}

	vv := v.Get("result")
	id := string(vv.GetStringBytes("id"))

	param["orderId"] = id

	return sgcpConfirm(zlog, param)

	/*

	 */
}

//转账确认
func sgcpConfirm(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {
	//确认存取款
	var pp fastjson.Parser

	confirmArgs := map[string]string{
		"agentID":  param["agent_id"].(string),
		"root":     param["merchant"].(string),
		"username": param["username"].(string),
	}

	conJsonData := map[string]string{
		"id": param["orderId"].(string),
	}

	conJsonStr, _ := jettison.Marshal(conJsonData)

	conParamStr := sgcpPack(param["agent_key"].(string), confirmArgs)
	confirmUrl := fmt.Sprintf("%s/api/confirm?%s", param["api"].(string), conParamStr)

	header := map[string]string{
		"Content-Type": "application/json",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, conJsonStr, SGCP, confirmUrl, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := pp.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	if !v.GetBool("success") {
		return Failure, string(v.GetStringBytes("message"))
	}
	return Success, param["orderId"].(string)
}
