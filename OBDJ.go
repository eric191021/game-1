package casino

import (
	"fmt"
	"net/url"
	"sort"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
)

const (
	OBDJ = "OBDJ"
)

func obdjPack(args map[string]string) string {

	i := 0
	qs := ""
	param := url.Values{}
	keys := make([]string, len(args))

	for k, _ := range args {
		keys[i] = k
		i++
	}

	sort.Strings(keys)

	for _, v := range keys {
		qs += fmt.Sprintf("%s=%s&", v, args[v])
		param.Set(v, args[v])
	}

	qs = qs[:len(qs)-1]
	sign := getMD5Hash(qs)

	param.Del("key")
	param.Set("sign", sign)

	return param.Encode()
}

// 注册
func obdjReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"username": param["username"].(string),
		"password": param["password"].(string),
		"merchant": param["merchant"].(string),
		"tester":   "0",
		"key":      param["key"].(string),
		"time":     param["s"].(string),
	}

	if param["tester"].(string) == "0" {
		args["tester"] = "1"
	}

	str := obdjPack(args)
	url := fmt.Sprintf("%s/api/member/register?%s", param["url"], str)

	statusCode, body, err := httpGetWithPushLog(zlog, OBDJ, url)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("status")) == "true" || string(v.GetStringBytes("data")) == "the username is already registered" {
		return Success, "success"
	}

	return Failure, string(v.GetStringBytes("data"))
}

func obdjLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"username": param["username"].(string),
		"password": param["password"].(string),
		"merchant": param["merchant"].(string),
		"key":      param["key"].(string),
		"time":     param["s"].(string),
	}

	str := obdjPack(args)
	url := fmt.Sprintf("%s/api/member/login?%s", param["url"].(string), str)

	statusCode, body, err := httpGetWithPushLog(zlog, OBDJ, url)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("status")) == "true" {

		host := param["web"].(string)
		if param["deviceType"].(string) != "1" {
			host = param["h5"].(string)
		}
		return Success, fmt.Sprintf("%s/home?token=%s", host, string(v.GetStringBytes("data")))
	}

	return Failure, string(v.GetStringBytes("data"))
}

func obdjBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"username": param["username"].(string),
		"merchant": param["merchant"].(string),
		"key":      param["key"].(string),
		"time":     param["s"].(string),
	}

	str := obdjPack(args)
	url := fmt.Sprintf("%s/api/fund/getBalance?%s", param["url"].(string), str)

	statusCode, body, err := httpGetWithPushLog(zlog, OBDJ, url)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("status")) == "true" {
		balace, _ := GetBalanceFromByte(v.GetStringBytes("data"))
		return Success, balace
	}

	return Failure, string(v.GetStringBytes("data"))
}

func obdjTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	merOrderId := fmt.Sprintf("%s%s", param["id"].(string), param["s"].(string))

	args := map[string]string{
		"type":       "1",
		"amount":     param["amount"].(string),
		"merOrderId": merOrderId,
		"username":   param["username"].(string),
		"merchant":   param["merchant"].(string),
		"key":        param["key"].(string),
		"time":       param["s"].(string),
	}

	if param["type"].(string) == "out" {
		args["type"] = "2"
	}

	str := obdjPack(args)
	url := fmt.Sprintf("%s/api/fund/transfer?%s", param["url"], str)

	statusCode, body, err := httpGetWithPushLog(zlog, OBDJ, url)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("status")) == "true" {
		return Success, merOrderId
	}

	return Failure, string(v.GetStringBytes("data"))
}

//转账确认
func obdjConfirm(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	return Success, ""
}
