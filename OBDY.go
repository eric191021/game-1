package casino

import (
	b64 "encoding/base64"
	"encoding/hex"
	"fmt"
	"net/url"

	"github.com/shopspring/decimal"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
	"lukechampine.com/frand"
)

const (
	OBDY = "OBDY"
)

func obdyPack(qs url.Values, data map[string]string, aesKey string) (string, string) {

	qstr := fmt.Sprintf("%s%s%s", qs.Get("agent"), qs.Get("timestamp"), qs.Get("key"))
	sign := getMD5Hash(qstr)
	r := hex.EncodeToString(frand.Bytes(16))

	str := r[:2]

	for i, c := range sign {
		if i == 9 {
			str += r[2:4] + string(c)
		} else if i == 17 {
			str += r[4:6] + string(c)
		} else {
			str += string(c)
		}
	}

	str += r[6:8]

	s, _ := jettison.Marshal(data)
	encrypted := aesCbcEncrypt(s, qs.Get("key"), aesKey)
	sEnc := b64.StdEncoding.EncodeToString(encrypted)

	qs.Del("key")
	qs.Set("sign", str)

	return qs.Encode(), sEnc
}

func obdyReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {
	return obdyLogin(zlog, param)
}

func obdyLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	gameId := "200"

	if _, ok := param["gamecode"].(string); ok {
		gameId = param["gamecode"].(string)
	}

	qs := url.Values{}

	qs.Set("agent", param["merchant"].(string))
	qs.Set("randno", param["s"].(string))
	qs.Set("timestamp", param["s"].(string))
	qs.Set("key", param["key"].(string))

	data := map[string]string{
		"memberId":   param["username"].(string),
		"memberName": param["username"].(string),
		"memberPwd":  param["password"].(string),
		"deviceType": param["deviceType"].(string),
		"memberIp":   param["ip"].(string),
		"gameId":     gameId,
	}

	uri, postbody := obdyPack(qs, data, param["iv"].(string))

	url := fmt.Sprintf("%s/launchGameById?%s", param["url"].(string), uri)

	header := map[string]string{
		"Content-Type": "text/plain",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(postbody), OBDY, url, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("code") == 1000 {
		return Success, string(v.GetStringBytes("data"))
	}
	return Failure, string(v.GetStringBytes("msg"))
}

func obdyBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {
	var p fastjson.Parser

	qs := url.Values{}

	qs.Set("agent", param["merchant"].(string))
	qs.Set("randno", param["s"].(string))
	qs.Set("timestamp", param["s"].(string))
	qs.Set("key", param["key"].(string))

	data := map[string]string{
		"memberId":  param["username"].(string),
		"memberPwd": param["password"].(string),
		"memberIp":  param["ip"].(string),
	}

	uri, postbody := obdyPack(qs, data, param["iv"].(string))

	url := fmt.Sprintf("%s/queryBalance?%s", param["url"].(string), uri)

	header := map[string]string{
		"Content-Type": "text/plain",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(postbody), OBDY, url, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("code") == 1000 {
		vv := v.Get("data")
		balance := decimal.NewFromFloat(vv.GetFloat64("balance")).Div(decimal.NewFromInt(100))
		return Success, balance.StringFixed(2)
	}
	return Failure, string(v.GetStringBytes("msg"))
}

func obdyTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {
	var p fastjson.Parser

	qs := url.Values{}

	qs.Set("agent", param["merchant"].(string))
	qs.Set("randno", param["s"].(string))
	qs.Set("timestamp", param["s"].(string))
	qs.Set("key", param["key"].(string))

	orderId := fmt.Sprintf("%s:%s", param["username"].(string), param["ms"].(string))

	//额度转成分
	amount, _ := decimal.NewFromString(param["amount"].(string))

	cent := amount.Mul(decimal.NewFromInt(100))

	data := map[string]string{
		"memberId":   param["username"].(string),
		"memberName": param["username"].(string),
		"memberPwd":  param["password"].(string),
		"memberIp":   param["ip"].(string),
		"money":      cent.String(),
		"orderId":    orderId,
	}

	actionType := "transferIn"

	if param["type"].(string) == "out" {
		actionType = "transferOut"
	}

	uri, postbody := obdyPack(qs, data, param["iv"].(string))

	url := fmt.Sprintf("%s/%s?%s", param["url"].(string), actionType, uri)

	header := map[string]string{
		"Content-Type": "text/plain",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(postbody), OBDY, url, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("code") == 1000 {
		return Success, orderId
	}
	return Failure, string(v.GetStringBytes("msg"))
}

//转账确认
func obdyConfirm(zlog *fluent.Fluent, param map[string]interface{}) bool {

	return false
}
