package casino

import (
	"fmt"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
)

const BGZR string = "BGZR"

//BG真人
func bgzrPack(args map[string]string, id, method string) []byte {

	bs, _ := jettison.Marshal(map[string]interface{}{
		"id":      id,
		"method":  method,
		"params":  args,
		"jsonrpc": "2.0",
	})

	return bs
}

func getSecretCode() string {
	return Base64Encode(Sha1Sum("yk123456"))
}

// 注册
func bgzrReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	rd := randomString(32)
	digest := getMD5Hash(rd + param["sn"].(string) + getSecretCode())

	method := "open.user.create"
	requestURI := fmt.Sprintf("%s/%s", param["api"].(string), method)
	header := map[string]string{
		"Content-Type": "application/json",
	}

	requestBody := bgzrPack(map[string]string{
		"random":       rd,
		"digest":       digest,
		"sn":           param["sn"].(string),
		"agentLoginId": param["agent_id"].(string),
		"loginId":      param["username"].(string),
	}, rd, method)

	statusCode, body, err := httpPostWithPushLog(zlog, requestBody, BGZR, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetBool("success") == true {
		return Success, "success"
	}

	if err := v.GetObject("error"); err != nil {
		return Failure, err.Get("message").String()
	}

	return Failure, "failed"
}

// 登陆
func bgzrLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	rd := randomString(32)

	// 测试用户
	//if param["tester"].(string) == "1" {
	//
	//	sign := getMD5Hash(rd + param["sn"].(string) + param["secret_key"].(string))
	//	method = "open.video.trial.game.url"
	//	requestBody = bgzrPack(map[string]string{
	//		"random": rd,
	//		"sn":     param["sn"].(string),
	//		"sign":   sign,
	//	}, "31bd5547-4eff-4122-8f40-76dfeb7c4ddc", method)
	//
	//	requestURI = fmt.Sprintf("%s/%s", param["api"].(string), method)
	//} else { //正式用户

	digest := getMD5Hash(rd + param["sn"].(string) + param["username"].(string) + getSecretCode())
	method := "open.video.game.url"
	requestBody := bgzrPack(map[string]string{
		"random":  rd,
		"sn":      param["sn"].(string),
		"digest":  digest,
		"loginId": param["username"].(string),
	}, rd, method)

	requestURI := fmt.Sprintf("%s/%s", param["api"].(string), method)
	//}

	header := map[string]string{
		"Content-Type": "application/json",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, requestBody, BGZR, requestURI, header)

	if err != nil {

		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if err := v.GetObject("error"); err != nil {
		return Failure, err.Get("message").String()
	}

	return Success, string(v.GetStringBytes("result"))
}

// 查询余额
func bgzrBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var (
		p fastjson.Parser
	)
	rd := randomString(32)
	digest := getMD5Hash(rd + param["sn"].(string) + param["username"].(string) + getSecretCode())

	method := "open.balance.get"
	requestURI := fmt.Sprintf("%s/%s", param["api"].(string), method)
	header := map[string]string{
		"Content-Type": "application/json",
	}

	requestBody := bgzrPack(map[string]string{
		"random":  rd,
		"digest":  digest,
		"sn":      param["sn"].(string),
		"loginId": param["username"].(string),
	}, rd, method)

	statusCode, body, err := httpPostWithPushLog(zlog, requestBody, BGZR, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if err := v.GetObject("error"); err != nil {
		return Failure, err.Get("message").String()
	}

	return Success, GetBalanceFromFloat(v.GetFloat64("result"))
}

// 上下分
func bgzrTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	rd := randomString(32)

	args := map[string]string{
		"random":  rd,
		"sn":      param["sn"].(string),
		"loginId": param["username"].(string),
	}
	if param["type"].(string) == "in" {
		args["amount"] = param["amount"].(string)
	} else {
		args["amount"] = "-" + param["amount"].(string)
	}

	digest := getMD5Hash(rd + param["sn"].(string) + param["username"].(string) + args["amount"] + getSecretCode())
	args["digest"] = digest

	method := "open.balance.transfer"
	requestURI := fmt.Sprintf("%s/%s", param["api"].(string), method)
	header := map[string]string{
		"Content-Type": "application/json",
	}
	requestBody := bgzrPack(args, rd, method)

	statusCode, body, err := httpPostWithPushLog(zlog, requestBody, BGZR, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if err := v.GetObject("error"); err != nil {
		return Failure, err.Get("message").String()
	}

	return Success, fmt.Sprintf("%f", v.GetFloat64("result"))
}
