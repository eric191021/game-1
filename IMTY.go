package casino

import (
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/bet365/jingo"
	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
)

const (
	dateF = "2006-01-02 15:04:05"
	IMTY  = "IMTY"
)

type imtyResponse struct {
	ValidateSFTokenResult int    `json:"ValidateSFTokenResult"`
	MemberCode            string `json:"memberCode"`
	Currency              string `json:"currency"`
	IpAddress             string `json:"ipAddress"`
	StatusCode            int    `json:"statusCode"`
	StatusDesc            string `json:"statusDesc"`
}

func imtyPack(key string) string {

	has := md5.Sum([]byte(key))
	key = fmt.Sprintf("%x", has)

	lens := len(key) / 2
	md5raw := ""
	for i := 0; i < lens; i++ {
		hexByte, _ := hex.DecodeString(substring(key, i*2, (i*2)+2))
		md5raw = md5raw + string(hexByte)
	}

	nt := time.Now().Unix()
	preDayTime := nt - 12*3600
	nano := time.Now().UnixNano()
	nanoStr := strconv.FormatInt(nano, 10)
	timeStr := time.Unix(preDayTime, 0).Format(dateF)

	timeStamp := timeStr + "." + substring(nanoStr, len(nanoStr)-3, len(nanoStr))
	aesByte := aesEcbEncrypt([]byte(timeStamp), []byte(md5raw))

	return base64.StdEncoding.EncodeToString(aesByte)
}

// 登录回调
func ImtyCallback(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {
	state := 0
	msg := "token null"
	username := ""
	ip := ""

	keyB := []byte(param["key"].(string))
	keyByte := make([]byte, 8) //设置加密数组

	tokenParam, ok := param["token"].(string)

	if !ok || tokenParam == "" {
		return Failure, msg
	}

	copy(keyByte[0:], keyB[0:8])

	strByte, _ := base64.StdEncoding.DecodeString(tokenParam)

	tokenByte, _ := desDecrypt(strByte, keyByte)
	token := string(tokenByte)

	msg = "token error"
	tokenArr := strings.Split(token, "-")
	if len(tokenArr) == 2 {
		username = tokenArr[1]
	}

	arr := strings.Split(tokenArr[0], "_")
	if len(arr) == 2 {
		ip = arr[1]
	}

	if username != "" {

		msg = "Success"
		state = 100

	}

	res := imtyResponse{
		ValidateSFTokenResult: state,
		MemberCode:            username,
		Currency:              param["currency"].(string),
		IpAddress:             ip,
		StatusCode:            state,
		StatusDesc:            msg,
	}

	enc := jingo.NewStructEncoder(imtyResponse{})

	buf := jingo.NewBufferFromPool()

	enc.Marshal(&res, buf)

	b := buf.String()
	buf.ReturnToPool()

	return Success, b
}

// 注册
func imtyReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	str := imtyPack(param["key"].(string))

	tokenStr := fmt.Sprintf("IMTY_%s-%s", param["ip"].(string), param["username"].(string))

	keyB := []byte(param["key"].(string))
	keyByte := make([]byte, 8) //设置加密数组

	copy(keyByte[0:], keyB[0:8])

	token, _ := desEncrypt([]byte(tokenStr), keyByte)

	args := map[string]string{
		"timeStamp": str,
		"token":     token,
	}

	jsonStr, _ := jettison.Marshal(args)

	u, _ := url.Parse(param["api"].(string))

	requestURI := fmt.Sprintf("%s/api/login", param["api"].(string))

	headers := map[string]string{
		"Content-Type": "application/json",
		"Accept":       "application/json",
		"Host":         u.Host,
		"Connection":   "Keep-Alive",
	}
	statusCode, body, err := httpPostWithPushLog(zlog, jsonStr, IMTY, requestURI, headers)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	if v.GetFloat64("statusCode") == 100 {
		msg := fmt.Sprintf(`{"token":"%s","timestamp":"%s"}`, token, str)
		return Success, msg
	}

	return Failure, string(v.GetStringBytes("statusDesc"))
}

func imtyLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	res, msg := imtyReg(zlog, param)

	if Success != res {
		return res, msg
	}

	v, err := p.Parse(msg)

	if err != nil {
		return Failure, err.Error()
	}

	token := string(v.GetStringBytes("token"))

	timestamp := string(v.GetStringBytes("timestamp"))

	lang := map[string]string{
		"1": "ZH",
	}

	loginUrl := fmt.Sprintf("%s/?timestamp=%s&token=%s&LanguageCode=%s", param["login_url"].(string), timestamp, token, lang[param["lang"].(string)])
	return Success, loginUrl

}

func imtyBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	str := imtyPack(param["key"].(string))

	args := map[string]string{
		"timeStamp":    str,
		"memberCode":   param["username"].(string),
		"currencyCode": param["currency"].(string),
	}

	jsonStr, _ := jettison.Marshal(args)

	u, _ := url.Parse(param["api"].(string))

	requestURI := fmt.Sprintf("%s/api/getmemberbalance", param["api"].(string))

	headers := map[string]string{
		"Content-Type": "application/json",
		"Accept":       "application/json",
		"Host":         u.Host,
		"Connection":   "Keep-Alive",
	}
	statusCode, body, err := httpPostWithPushLog(zlog, jsonStr, IMTY, requestURI, headers)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	if v.GetFloat64("statusCode") == 100 {
		return Success, fmt.Sprintf("%.2f", v.GetFloat64("amount"))
	}

	return Failure, string(v.GetStringBytes("statusDesc"))
}

func imtyTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	str := imtyPack(param["key"].(string))

	actionTypeId := "1"

	if param["type"].(string) == "out" {
		actionTypeId = "2"
	}

	args := map[string]string{
		"timeStamp":    str,
		"memberCode":   param["username"].(string),
		"currencyCode": param["currency"].(string),
		"amount":       param["amount"].(string),
		"actionTypeId": actionTypeId,
		"referenceId":  param["id"].(string),
	}

	jsonStr, _ := jettison.Marshal(args)

	u, _ := url.Parse(param["api"].(string))

	requestURI := fmt.Sprintf("%s/api/fundtransfer", param["api"].(string))

	headers := map[string]string{
		"Content-Type": "application/json",
		"Accept":       "application/json",
		"Host":         u.Host,
		"Connection":   "Keep-Alive",
	}
	statusCode, body, err := httpPostWithPushLog(zlog, jsonStr, IMTY, requestURI, headers)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	if v.GetFloat64("statusCode") == 100 {
		return Success, param["id"].(string)
	}

	return Failure, string(v.GetStringBytes("statusDesc"))
}

//转账确认
func imtyConfirm(zlog *fluent.Fluent, param map[string]interface{}) bool {

	return false
}

//获取source的子串,如果start小于0或者end大于source长度则返回""
//start:开始index，从0开始，包括0
//end:结束index，以end结束，但不包括end
func substring(source string, start int, end int) string {

	var r = []rune(source)
	length := len(r)

	if start < 0 || end > length || start > end {
		return ""
	}

	if start == 0 && end == length {
		return source
	}

	return string(r[start:end])
}
