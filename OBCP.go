package casino

import (
	"fmt"
	"net/url"
	"sort"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
)

const OBCP string = "OBCP"

func obcpPack(key string, args map[string]string, isJson bool) string {

	i := 0
	qs := ""
	param := url.Values{}
	data := map[string]string{}
	keys := make([]string, len(args))

	for k, _ := range args {
		keys[i] = k
		i++
	}

	sort.Strings(keys)

	for _, v := range keys {
		qs += v + args[v]

		param.Set(v, args[v])
		data[v] = args[v]
	}

	qs += key
	sign := getMD5Hash(qs)

	param.Set("sign", sign)
	data["sign"] = sign

	s, _ := jettison.Marshal(data)
	if isJson {
		return string(s)
	}

	return param.Encode()
}

// 注册
func obcpReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"member":     param["username"].(string),
		"merchant":   param["merchant"].(string),
		"password":   param["password"].(string),
		"doubleList": "",
		"normalList": "",
		"memberType": "1",
		"timestamp":  param["ms"].(string),
	}

	if param["tester"] == "0" {
		args["memberType"] = "3"
	}
	url := fmt.Sprintf("%s/boracay/api/member/create", param["url"].(string))
	str := obcpPack(param["key"].(string), args, true)

	header := map[string]string{
		"Content-Type": "application/json",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), OBCP, url, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("code") == 0 || v.GetInt("code") == 701 {
		return Success, "success"
	}

	return Failure, string(v.GetStringBytes("msg"))
}

func obcpLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"member":    param["username"].(string),
		"merchant":  param["merchant"].(string),
		"password":  param["password"].(string),
		"timestamp": param["ms"].(string),
	}

	url := fmt.Sprintf("%s/boracay/api/member/login", param["url"].(string))
	str := obcpPack(param["key"].(string), args, false)

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), OBCP, url, nil)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("code") == 0 {

		host := param["web"].(string)
		if param["deviceType"] != "1" {
			host = param["h5"].(string)
		}

		vv := v.Get("data")

		return Success, fmt.Sprintf("%s/?token=%s", host, string(vv.GetStringBytes("token")))
	}

	return Failure, string(v.GetStringBytes("msg"))
}

func obcpBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"member":    param["username"].(string),
		"merchant":  param["merchant"].(string),
		"timestamp": param["ms"].(string),
	}

	url := fmt.Sprintf("%s/boracay/api/nofreemember/balanceQuery", param["url"].(string))
	str := obcpPack(param["key"].(string), args, true)

	header := map[string]string{
		"Content-Type": "application/json",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), OBCP, url, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("code") == 0 {

		vv := v.Get("data")
		amount, _ := GetBalanceFromByte(vv.GetStringBytes("amount"))
		return Success, amount
	}

	return Failure, string(v.GetStringBytes("msg"))
}

func obcpTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"member":          param["username"].(string),
		"amount":          param["amount"].(string),
		"merchantAccount": param["merchant"].(string),
		"notifyId":        param["id"].(string),
		"transferType":    "1",
		"timestamp":       param["ms"].(string),
	}

	if param["type"] == "out" {
		args["transferType"] = "2"
	}

	url := fmt.Sprintf("%s/boracay/api/nofreemember/transferBalance", param["url"].(string))
	str := obcpPack(param["key"].(string), args, true)

	header := map[string]string{
		"Content-Type": "application/json",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), OBCP, url, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("code") == 0 {
		return Success, args["notifyId"]
	}

	return Failure, string(v.GetStringBytes("msg"))
}

//转账确认
func obcpConfirm(zlog *fluent.Fluent, param map[string]interface{}) int {

	return Success
}
