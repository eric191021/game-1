package casino

import (
	"fmt"
	"time"

	"github.com/shopspring/decimal"

	"github.com/fluent/fluent-logger-golang/fluent"
	jsoniter "github.com/json-iterator/go"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttpproxy"
)

type callFunc func(*fluent.Fluent, map[string]interface{}) (int, string)

var (
	timeout time.Duration
	fc      *fasthttp.Client
	cjson   = jsoniter.ConfigCompatibleWithStandardLibrary
)

const (
	apiTimeOut = time.Second * 12
)

func pushLog(zlog *fluent.Fluent, requestBody []byte, name, requestURI string, code int, body []byte, err error) {
	l := log_t{
		Requesturl:  requestURI,
		Requestbody: string(requestBody),
		Statuscode:  code,
		Body:        string(body),
		Err:         "",
		Level:       "info",
		Name:        name,
	}
	if err != nil {
		l.Err = err.Error()
		l.Level = "warm"
	}
	err = zlog.Post(tag, l)
	if err != nil {
		fmt.Printf("Push Platform Log is error: %s \n", err.Error())
	}
}

func httpPostWithPushLog(zlog *fluent.Fluent, requestBody []byte, name, requestURI string, headers map[string]string) (int, []byte, error) {
	statusCode, body, err := httpPost(requestBody, requestURI, headers)
	pushLog(zlog, requestBody, name, requestURI, statusCode, body, err)
	return statusCode, body, err
}

func httpGetWithPushLog(zlog *fluent.Fluent, name, requestURI string) (int, []byte, error) {
	statusCode, body, err := httpGet(requestURI)
	pushLog(zlog, nil, name, requestURI, statusCode, body, err)
	return statusCode, body, err
}

func httpGetHeaderWithLog(zlog *fluent.Fluent, name, requestURI string, headers map[string]string) (int, []byte, error) {
	statusCode, body, err := httpGetHeader(requestURI, headers)
	pushLog(zlog, nil, name, requestURI, statusCode, body, err)
	return statusCode, body, err
}

func httpPost(requestBody []byte, requestURI string, headers map[string]string) (int, []byte, error) {

	req := fasthttp.AcquireRequest()
	resp := fasthttp.AcquireResponse()

	defer func() {
		fasthttp.ReleaseResponse(resp)
		fasthttp.ReleaseRequest(req)
	}()

	req.SetRequestURI(requestURI)
	req.Header.SetMethod("POST")
	req.SetBody(requestBody)

	//req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	if headers != nil {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}

	err := fc.DoTimeout(req, resp, apiTimeOut)
	return resp.StatusCode(), resp.Body(), err
}

func httpGet(requestURI string) (int, []byte, error) {

	return fc.GetTimeout(nil, requestURI, apiTimeOut)
}

func httpGetHeader(requestURI string, headers map[string]string) (int, []byte, error) {

	req := fasthttp.AcquireRequest()
	resp := fasthttp.AcquireResponse()

	defer func() {
		fasthttp.ReleaseResponse(resp)
		fasthttp.ReleaseRequest(req)
	}()

	req.SetRequestURI(requestURI)
	req.Header.SetMethod("GET")
	//req.SetBody(requestBody)
	//req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	if headers != nil {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}

	err := fc.DoTimeout(req, resp, apiTimeOut)

	return resp.StatusCode(), resp.Body(), err
}

func GetBalanceFromString(balance string) (string, error) {
	deciBalance, err := decimal.NewFromString(balance)
	if err != nil {
		return "", err
	}
	result := deciBalance.StringFixed(4)
	return result[:len(result)-2], nil
}

func GetBalanceFromByte(balance []byte) (string, error) {
	deciBalance, err := decimal.NewFromString(string(balance))
	if err != nil {
		return "", err
	}
	result := deciBalance.StringFixed(4)
	return result[:len(result)-2], nil
}

func GetBalanceFromFloat(balance float64) string {
	deciBalance := decimal.NewFromFloat(balance)
	result := deciBalance.StringFixed(4)
	return result[:len(result)-2]
}

func New(socks5 string) {

	fc = &fasthttp.Client{
		ReadTimeout:  apiTimeOut,
		WriteTimeout: apiTimeOut,
	}

	if socks5 != "0.0.0.0" {
		fc.Dial = fasthttpproxy.FasthttpSocksDialer(socks5)
	}
}

func Dispatch(zlog *fluent.Fluent, name string, param map[string]interface{}) (int, string) {

	if rule, ok := rules[name]; ok {
		name, ok1 := validator(rule, param)
		if !ok1 {
			return Failure, name + " Not Valid"
		}
	} else {
		return Failure, "Not Found Method"
	}

	act := fmt.Sprintf("%s_%s", param["name"], name)
	if cb, ok := routes[act]; ok {
		return cb(zlog, param)
	}

	return Failure, "Not Found Method"
}
