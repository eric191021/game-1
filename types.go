package casino

const (
	Failure    int = 0 //失败
	Success    int = 1 //成功
	Pending    int = 3 //待处理
	Processing int = 2 //处理中
)

const tag string = "platform"

type log_t struct {
	Requesturl  string `json:"requestURL"`
	Requestbody string `json:"requestBody"`
	Statuscode  int    `json:"statusCode"`
	Body        string `json:"body"`
	Level       string `json:"level"`
	Err         string `json:"err"`
	Name        string `json:"name"`
}

var routes = map[string]callFunc{
	// ob体育
	"obty_reg":      obtyReg,
	"obty_login":    obtyLogin,
	"obty_balance":  obtyBalance,
	"obty_transfer": obtyTransfer,

	// ob彩票
	"obcp_reg":      obcpReg,
	"obcp_login":    obcpLogin,
	"obcp_balance":  obcpBalance,
	"obcp_transfer": obcpTransfer,

	// ob电竞
	"obdj_reg":      obdjReg,
	"obdj_login":    obdjLogin,
	"obdj_balance":  obdjBalance,
	"obdj_transfer": obdjTransfer,

	// ob真人
	"obzr_reg":      obzrReg,
	"obzr_login":    obzrLogin,
	"obzr_balance":  obzrBalance,
	"obzr_transfer": obzrTransfer,

	// ob棋牌
	"obqp_reg":      obqpReg,
	"obqp_login":    obqpLogin,
	"obqp_balance":  obqpBalance,
	"obqp_transfer": obqpTransfer,

	// ob电游
	"obdy_reg":      obdyReg,
	"obdy_login":    obdyLogin,
	"obdy_balance":  obdyBalance,
	"obdy_transfer": obdyTransfer,

	// ebet
	"ebet_reg":      ebetReg,
	"ebet_login":    ebetLogin,
	"ebet_balance":  ebetBalance,
	"ebet_transfer": ebetTransfer,
	"ebet_confirm":  ebetConfirm,

	// 雷火电竞
	"tfdj_reg":      tfdjReg,
	"tfdj_login":    tfdjLogin,
	"tfdj_balance":  tfdjBalance,
	"tfdj_transfer": tfdjTransfer,

	// 完美真人
	"wmzr_reg":      wmzrReg,
	"wmzr_login":    wmzrLogin,
	"wmzr_balance":  wmzrBalance,
	"wmzr_transfer": wmzrTransfer,

	// tcg彩票
	"tcgcp_reg":      tcgcpReg,
	"tcgcp_login":    tcgcpLogin,
	"tcgcp_balance":  tcgcpBalance,
	"tcgcp_transfer": tcgcpTransfer,

	//双赢彩票
	"sgcp_reg":      sgcpReg,
	"sgcp_login":    sgcpLogin,
	"sgcp_balance":  sgcpBalance,
	"sgcp_transfer": sgcpTransfer,

	//IM棋牌
	"imqp_reg":      imqpReg,
	"imqp_login":    imqpLogin,
	"imqp_balance":  imqpBalance,
	"imqp_transfer": imqpTransfer,

	//PG电子
	"pgdy_reg":      pgdyReg,
	"pgdy_login":    pgdyLogin,
	"pgdy_balance":  pgdyBalance,
	"pgdy_transfer": pgdyTransfer,

	//AG真人/AG捕鱼
	"agzr_reg":      agzrReg,
	"agzr_login":    agzrLogin,
	"agzr_balance":  agzrBalance,
	"agzr_transfer": agzrTransfer,
	"agzr_confirm":  agzrConfirm,

	//BG真人
	"bgzr_reg":      bgzrReg,
	"bgzr_login":    bgzrLogin,
	"bgzr_balance":  bgzrBalance,
	"bgzr_transfer": bgzrTransfer,

	//IM体育
	"imty_reg":      imtyReg,
	"imty_login":    imtyLogin,
	"imty_balance":  imtyBalance,
	"imty_transfer": imtyTransfer,

	//IM电竞
	"imdj_reg":      imdjReg,
	"imdj_login":    imdjLogin,
	"imdj_balance":  imdjBalance,
	"imdj_transfer": imdjTransfer,

	//OB捕鱼
	// "obby_reg":      obbyReg,
	// "obby_login":    obbyLogin,
	// "obby_balance":  obbyBalance,
	// "obby_transfer": obbyTransfer,
}
