package casino

import (
	b64 "encoding/base64"
	"fmt"
	"strings"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
)

const (
	OBZR = "OBZR"
)

func obzrPack(data map[string]string, merchant, md5key, aesKey string) string {

	text, _ := jettison.Marshal(data)
	encrypted := aesEcbEncrypt(text, []byte(aesKey))
	sEnc := b64.StdEncoding.EncodeToString(encrypted)

	qstr := fmt.Sprintf("%s%s", text, md5key)
	sign := getMD5Hash(qstr)

	result := map[string]string{
		"merchantCode": merchant,
		"params":       sEnc,
		"signature":    strings.ToUpper(sign),
	}

	s, _ := jettison.Marshal(result)

	return string(s)
}

// 注册
func obzrReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"lang":          param["lang"].(string),
		"nickName":      param["username"].(string),
		"loginName":     param["merchant"].(string) + param["username"].(string),
		"loginPassword": param["password"].(string),
		"merchant":      param["merchant"].(string),
		"timestamp":     param["ms"].(string),
	}

	str := obzrPack(args, param["merchant"].(string), param["key"].(string), param["iv"].(string))
	url := fmt.Sprintf("%s/api/merchant/create/v2", param["url"].(string))

	header := map[string]string{
		"Content-Type": "application/json",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), OBZR, url, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("code")) == "200" || string(v.GetStringBytes("code")) == "20000" {
		return Success, "success"
	}

	return Failure, string(v.GetStringBytes("message"))
}

func obzrLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"lang":          param["lang"].(string),
		"nickName":      param["username"].(string),
		"loginName":     param["merchant"].(string) + param["username"].(string),
		"loginPassword": param["password"].(string),
		"merchant":      param["merchant"].(string),
		"showExit":      "0",
		"deviceType":    param["deviceType"].(string),
		"backurl":       param["backurl"].(string),
		"timestamp":     param["ms"].(string),
	}

	str := obzrPack(args, param["merchant"].(string), param["key"].(string), param["iv"].(string))
	url := fmt.Sprintf("%s/api/merchant/forwardGame/v2", param["url"].(string))

	header := map[string]string{
		"Content-Type": "application/json",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), OBZR, url, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("code")) == "200" {

		vv := v.Get("data")
		return Success, string(vv.GetStringBytes("url"))
	}

	return Failure, string(v.GetStringBytes("message"))
}

func obzrBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"loginName": param["merchant"].(string) + param["username"].(string),
		"timestamp": param["ms"].(string),
	}

	str := obzrPack(args, param["merchant"].(string), param["key"].(string), param["iv"].(string))
	url := fmt.Sprintf("%s/api/merchant/balance/v1", param["url"].(string))

	header := map[string]string{
		"Content-Type": "application/json",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), OBZR, url, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("code")) == "200" {

		vv := v.Get("data")
		res, _ := GetBalanceFromByte(vv.GetStringBytes("balance"))
		return Success, res
	}

	return Failure, string(v.GetStringBytes("message"))
}

func obzrTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"transferNo": param["id"].(string),
		"amount":     param["amount"].(string),
		"loginName":  param["merchant"].(string) + param["username"].(string),
		"timestamp":  param["ms"].(string),
	}

	str := obzrPack(args, param["merchant"].(string), param["key"].(string), param["iv"].(string))
	url := fmt.Sprintf("%s/api/merchant/deposit/v1", param["url"].(string))
	if param["type"] == "out" {
		url = fmt.Sprintf("%s/api/merchant/withdraw/v1", param["url"].(string))
	}

	header := map[string]string{
		"Content-Type": "application/json",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), OBZR, url, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("code")) == "200" {
		vv := v.Get("request")
		return Success, string(vv.GetStringBytes("transferNo"))
	}

	return Failure, string(v.GetStringBytes("message"))
}

//转账确认
func obzrConfirm(zlog *fluent.Fluent, param map[string]interface{}) (bool, string) {

	return false, ""
}
