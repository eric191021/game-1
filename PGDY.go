package casino

import (
	"fmt"
	"net/url"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
)

const (
	PGDY = "PGDY"
)

func pgdyPack(args map[string]string) string {

	p := url.Values{}

	for k, v := range args {
		p.Set(k, v)
	}
	return p.Encode()
}

func pgdyReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"secret_key":     param["secret_key"].(string),
		"operator_token": param["operator_token"].(string),
		"player_name":    param["username"].(string),
		"currency":       param["currency"].(string),
	}
	postData := pgdyPack(args)
	reqUrl := fmt.Sprintf("%s/Player/Create?%s", param["api"].(string), postData)

	u, _ := url.Parse(param["api"].(string))
	header := map[string]string{
		"Host": u.Host,
	}
	statusCode, body, err := httpPostWithPushLog(zlog, nil, PGDY, reqUrl, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	vv := v.Get("data")

	if string(vv.GetStringBytes("action_result")) != "Success" {
		return Failure, string(vv.GetStringBytes("msg"))
	}

	verr := v.Get("error")
	if verr.String() == "null" || string(verr.GetStringBytes("code")) == "9411" {
		return Success, "success"
	}
	return Failure, string(vv.GetStringBytes("msg"))
}

func pgdyLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"secret_key":     param["secret_key"].(string),
		"operator_token": param["operator_token"].(string),
		"player_name":    param["username"].(string),
		"game_code":      param["gamecode"].(string),
		// "game_code": "diaochan",//测试的游戏启动
	}
	postData := pgdyPack(args)
	reqUrl := fmt.Sprintf("%s/Launch?%s", param["api"].(string), postData)
	u, _ := url.Parse(param["api"].(string))
	header := map[string]string{
		"Host": u.Host,
	}
	statusCode, body, err := httpPostWithPushLog(zlog, nil, PGDY, reqUrl, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	verr := v.Get("error")
	if verr.String() != "null" {
		return Failure, string(verr.GetStringBytes("message"))
	}

	vv := v.Get("data")

	return Success, string(vv.GetStringBytes("game_url"))
}

func pgdyBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	args := map[string]string{
		"secret_key":     param["secret_key"].(string),
		"operator_token": param["operator_token"].(string),
		"player_name":    param["username"].(string),
	}
	postData := pgdyPack(args)
	reqUrl := fmt.Sprintf("%s/GetPlayerBalance?%s", param["api"].(string), postData)

	u, _ := url.Parse(param["api"].(string))
	header := map[string]string{
		"Host": u.Host,
	}
	statusCode, body, err := httpPostWithPushLog(zlog, nil, PGDY, reqUrl, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	verr := v.Get("error")

	if verr.String() != "null" {
		return Failure, string(verr.GetStringBytes("message"))
	}

	vv := v.Get("data")

	return Success, GetBalanceFromFloat(vv.GetFloat64("balance"))
}

func pgdyTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {
	var p fastjson.Parser
	args := map[string]string{
		"secret_key":     param["secret_key"].(string),
		"operator_token": param["operator_token"].(string),
		"player_name":    param["username"].(string),
		"amount":         param["amount"].(string),
		"traceId":        param["id"].(string),
	}
	postData := pgdyPack(args)
	reqUrl := fmt.Sprintf("%s/TransferIn?%s", param["api"].(string), postData)
	if param["type"].(string) == "out" {
		reqUrl = fmt.Sprintf("%s/TransferOut?%s", param["api"].(string), postData)
	}
	u, _ := url.Parse(param["api"].(string))
	header := map[string]string{
		"Host": u.Host,
	}
	statusCode, body, err := httpPostWithPushLog(zlog, nil, PGDY, reqUrl, header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	vv := v.Get("error")
	if vv.String() != "null" {
		return Failure, string(vv.GetStringBytes("message"))
	}

	return Success, param["id"].(string)
}

//转账确认
func pgdyConfirm(zlog *fluent.Fluent, param map[string]interface{}) bool {

	return false
}
