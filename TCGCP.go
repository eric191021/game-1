package casino

import (
	"fmt"
	"net/url"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
)

const (
	TCGCP = "TCGCP"
)

//tcg彩票
func tcgcpPack(args map[string]string, merchantCode, desKey, sha256Key string) string {

	params, _ := jettison.Marshal(args)
	encryptedParams, _ := desEncrypt(params, []byte(desKey))

	param := url.Values{}
	param.Set("merchant_code", merchantCode)
	param.Set("params", encryptedParams)
	param.Set("sign", sha256sum([]byte(encryptedParams+sha256Key)))

	return param.Encode()
}

//tcg彩票注册
func tcgcpReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	args := map[string]string{
		"method":   "cm",
		"username": param["username"].(string),
		"password": "1234567",
		"currency": param["currency"].(string),
	}
	header := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	requestBody := tcgcpPack(args, param["merchant_code"].(string), param["des_key"].(string), param["sha256_key"].(string))

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), TCGCP, param["api"].(string), header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("status") == 0 {
		return Success, "success"
	}

	return Failure, string(v.GetStringBytes("error_desc"))
}

//tcg彩票登录
func tcgcpLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	platform := "html5"
	if param["deviceType"].(string) == "1" {
		platform = "html5-desktop"
	}
	args := map[string]string{
		"method":           "lg",
		"username":         param["username"].(string),
		"product_type":     param["product_type"].(string),
		"game_mode":        param["tester"].(string), //会员帐户类型（1 =真实，0 =测试）
		"game_code":        "Lobby",                  //param["gamecode"].(string),
		"platform":         platform,
		"lottery_bet_mode": param["lottery_bet_mode"].(string), //彩票游戏模式
		//"series":           param["series"].(string),           //奖金组
	}

	//默认为html5-desktop(html5-desktop,html5
	//*注意：只有一些游戏支持这个请参考游戏列表
	//*中国彩票都支持 html5 和 html5-desktop
	//*东南亚彩票自动辨别手机或网页版
	//*越南彩票支持 WEB 和 MOBILE
	if _, ok := param["platform"].(string); ok {
		args["platform"] = param["platform"].(string)
	}
	//启动的页面,请参考彩票页面代码 (彩票使用)
	if _, ok := param["view"].(string); ok {
		args["view"] = param["view"].(string)
	}
	//玩家点击返回游戏的跳转地址（可用于返回到平台，仅支持彩票）
	if _, ok := param["backurl"].(string); ok {
		args["back_url"] = param["backurl"].(string)
	}
	lang := map[string]string{
		"1": "ZH_CN", //简体中文
		"2": "EN",    //英语
		"3": "TH",    //泰语
		"4": "VI",    //越南语
		"5": "ID",    //印尼语
		"6": "JA",    //日语
		"7": "KO",    //韩语
	}
	//语言 默认中文ZH_CN
	if _, ok := param["lang"].(string); ok {
		args["language"] = lang[param["lang"].(string)]
	}

	header := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	requestBody := tcgcpPack(args, param["merchant_code"].(string), param["des_key"].(string), param["sha256_key"].(string))

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), TCGCP, param["api"].(string), header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("status") == 0 {
		return Success, fmt.Sprintf("%s/%s", param["login_url"].(string), string(v.GetStringBytes("game_url")))
	}

	return Failure, string(v.GetStringBytes("error_desc"))
}

//tcg彩票余额
func tcgcpBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	args := map[string]string{
		"method":       "gb",
		"username":     param["username"].(string),
		"product_type": param["product_type"].(string),
	}
	header := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	requestBody := tcgcpPack(args, param["merchant_code"].(string), param["des_key"].(string), param["sha256_key"].(string))

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), TCGCP, param["api"].(string), header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("status") == 0 {
		return Success, fmt.Sprintf("%.2f", v.GetFloat64("balance"))
	}

	res, _ := GetBalanceFromByte(v.GetStringBytes("error_desc"))
	return Failure, res
}

//tcg彩票转账
func tcgcpTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	transType := map[string]string{
		"in":  "1",
		"out": "2",
	}
	args := map[string]string{
		"method":       "ft",
		"username":     param["username"].(string),
		"product_type": param["product_type"].(string),
		"fund_type":    transType[param["type"].(string)], //1 =存款，2 =提款
		"amount":       param["amount"].(string),
		"reference_no": param["id"].(string), //我司转账单号
	}
	header := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	requestBody := tcgcpPack(args, param["merchant_code"].(string), param["des_key"].(string), param["sha256_key"].(string))

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), TCGCP, param["api"].(string), header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("status") == 0 {
		return Success, param["id"].(string)
	}

	return Failure, string(v.GetStringBytes("error_desc"))
}

//转账确认
func tcgcpConfirm(zlog *fluent.Fluent, param map[string]interface{}) bool {

	return false
}
