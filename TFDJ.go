package casino

import (
	"fmt"
	"net/url"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
)

const (
	TFDJ = "TFDJ"
)

func tfdjPack(args map[string]string) string {

	param := url.Values{}
	for k, v := range args {
		param.Set(k, v)
	}

	return param.Encode()
}

//雷火电竞注册
func tfdjReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	//请求格式
	/*
		curl --location -g --request POST 'https://{{spi-url}}/api/v2/members/' \
		--header 'Content-Type: application/json' \
		--header 'Authorization: Token {{private_token}}' \
		--data-raw '{
			"member_code": "test2323"
		}'
	*/
	args := map[string]string{
		"member_code": param["username"].(string),
	}
	//场馆限额参数，可选
	if _, ok := param["limit_amount"].(string); ok {
		args["limit_amount_per_event"] = param["limit_amount"].(string)
	}

	requestURI := fmt.Sprintf("%s/api/v2/members/", param["api"].(string))
	header := map[string]string{
		"Content-Type":  "application/json",
		"Authorization": "Token " + param["private_token"].(string),
	}
	statusCode, _, err := httpPostWithPushLog(zlog, []byte(tfdjPack(args)), TFDJ, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusCreated {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	//响应格式
	/*
		{
		  "member_code": "test2323",
		  "limit_amount_per_event": null
		}
	*/

	return Success, "success"
}

//雷火电竞登录
func tfdjLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	return Success, fmt.Sprintf("%s/?auth=%s&token=%s", param["login_url"].(string), param["public_token"].(string), param["token"].(string))
}

//雷火电竞余额
func tfdjBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	requestURI := fmt.Sprintf("%s/api/v2/balance/?LoginName=%s", param["api"].(string), param["username"].(string))
	header := map[string]string{
		"Authorization": "Token " + param["private_token"].(string),
	}

	statusCode, body, err := httpGetHeaderWithLog(zlog, TFDJ, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	vv := v.GetArray("results")

	if vv == nil || len(vv) < 1 {
		return Success, "0.00"
	}

	return Success, fmt.Sprintf("%.2f", vv[0].GetFloat64("balance"))
}

//雷火电竞上下分
func tfdjTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	//请求格式

	args := map[string]string{
		"member":       param["username"].(string),
		"operator_id":  param["partner_id"].(string),
		"amount":       param["amount"].(string),
		"reference_no": param["id"].(string),
	}

	//请求url
	requestURI := ""
	if param["type"].(string) == "in" {
		requestURI = fmt.Sprintf("%s/api/v2/deposit/", param["api"].(string))
	} else {
		requestURI = fmt.Sprintf("%s/api/v2/withdraw/", param["api"].(string))
	}
	header := map[string]string{
		"Content-Type":  "application/json",
		"Authorization": "Token " + param["private_token"].(string),
	}

	requestBody, err := jettison.Marshal(args)
	if err != nil {
		return Failure, err.Error()
	}
	statusCode, body, err := httpPostWithPushLog(zlog, requestBody, TFDJ, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusCreated {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	//响应格式:
	/*
		{
		  "member": "test22",
		  "operator_id": "2",
		  "amount": 2000,
		  "reference_no": "1234",
		  "currency": "RMB",
		  "transaction_type": "deposit",
		  "balance_amount": 35101.74
		}
	*/

	return Success, string(body)
}

//转账确认
func tfdjConfirm(zlog *fluent.Fluent, param map[string]interface{}) bool {

	return false
}
